﻿
using PueritiaVT.Interfaces;
using PueritiaVT.View;
using PueritiaVT.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace PueritiaVT.Services
{
    public class WindowService : IWindowService
    {
        public IWindowService ParentWindowService { get; set; }
        public Window ChildView { get; set; }
        public Window CurrentView { get; set; }

        public bool? ShowDialogWindow(object DataContext)
        {            
            ChildView.DataContext = DataContext;           
            return ChildView.ShowDialog();
        }
        public WindowService(IWindowService parentWindowService = null)
        {
            ParentWindowService = parentWindowService;
            CurrentView = ParentWindowService?.ChildView;
        }
        public void CreateChildWindow<T>() where T : Window, new()
        {
            ChildView = new T();
        }
    }
}
