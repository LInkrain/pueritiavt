﻿using PueritiaVT.Interfaces;
using PueritiaVT.Properties;
using System.Windows;

namespace PueritiaVT.Services
{
    public class MsgBoxService : IMsgBoxService
    {
        public void ShowNotification(string message)
        {
            MessageBox.Show(message, Resources.Notification, MessageBoxButton.OK);
        }

        public bool AskForConfirmation(string message)
        {
            MessageBoxResult result = MessageBox.Show(message, Resources.AreYouSure, MessageBoxButton.OKCancel);
            return result.HasFlag(MessageBoxResult.OK);
        }
    }
}
