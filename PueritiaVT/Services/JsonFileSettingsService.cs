﻿using PueritiaVT.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PueritiaVT.Models;
using System.Runtime.Serialization.Json;
using System.IO;

namespace PueritiaVT.Services
{
    class JsonFileSettingsService : IFileSettingsService
    {
        public string FileExtension
        {
            get
            {
                return ".json";
            }
        }

        public SettingsApp Open(string filename)
        {
            SettingsApp testSettings = new SettingsApp();
            DataContractJsonSerializer jsonFormatter =
                new DataContractJsonSerializer(typeof(SettingsApp));
            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
                testSettings = jsonFormatter.ReadObject(fs) as SettingsApp;
            }

            return testSettings;
        }

        public void Save(string filename, SettingsApp testSettings)
        {
            DataContractJsonSerializer jsonFormatter =
                 new DataContractJsonSerializer(typeof(SettingsApp));
            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                jsonFormatter.WriteObject(fs, testSettings);
            }
        }
 
    }
}
