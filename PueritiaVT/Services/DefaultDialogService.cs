﻿using Microsoft.Win32;
using PueritiaVT.Interfaces;
using System.Windows;

namespace PueritiaVT.Services
{
    public class DefaultDialogService : IDialogService
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string DefaultExt { get; set; } = ".txt";
        public string Filter { get; set; } = "txt (.txt)|*.txt";

        public bool OpenFileDialog()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = this.Filter,
                DefaultExt = this.DefaultExt
            };
            if (openFileDialog.ShowDialog() == true)
            {
                FilePath = openFileDialog.FileName;
                FileName = openFileDialog.SafeFileName;
                return true;
            }
            return false;
        }

        public bool SaveFileDialog()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = this.Filter,
                DefaultExt = this.DefaultExt
            };
            if (saveFileDialog.ShowDialog() == true)
            {
                FilePath = saveFileDialog.FileName;
                return true;
            }
            return false;
        }

        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }
    }
}
