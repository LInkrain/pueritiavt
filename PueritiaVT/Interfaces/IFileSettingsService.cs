﻿using PueritiaVT.Models;

namespace PueritiaVT.Interfaces
{
    public interface IFileSettingsService
    {
        SettingsApp Open(string filename);
        void Save(string filename, SettingsApp testSettings);
        string FileExtension { get; }
    }
}
