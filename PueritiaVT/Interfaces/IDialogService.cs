﻿
namespace PueritiaVT.Interfaces
{
    public interface IDialogService
    {
        void ShowMessage(string message);   // показ сообщения
        string FilePath { get; set; }   // путь к выбранному файлу
        string FileName { get; set; }   //Название файла
        bool OpenFileDialog();  // открытие файла
        bool SaveFileDialog();  // сохранение файла
        string Filter { get; set; } //Фильтр
        string DefaultExt { get; set; } //Расширение файла
    }
}
