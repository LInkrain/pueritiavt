﻿
namespace PueritiaVT.Interfaces
{
   public  interface IMsgBoxService
    {
        void ShowNotification(string message);
        bool AskForConfirmation(string message);
    }
}
