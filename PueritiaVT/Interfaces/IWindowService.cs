﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace PueritiaVT.Interfaces
{
   public interface IWindowService
    {
        IWindowService ParentWindowService { get; set; }
        Window ChildView { get; set; }
        Window CurrentView { get; set; }
        bool? ShowDialogWindow(object DataContext);
        void CreateChildWindow<T>() where T : Window, new();
    }
}
