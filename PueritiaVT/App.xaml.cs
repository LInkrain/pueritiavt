﻿using PueritiaVT.Services;
using PueritiaVT.View;
using PueritiaVT.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace PueritiaVT
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            WindowService windowService = new WindowService();
            windowService.CreateChildWindow<MainWindow>();
            MainMenuViewModel viewModel = new MainMenuViewModel(windowService, new DefaultDialogService(), new MsgBoxService(), new JsonFileSettingsService());
            windowService.ShowDialogWindow(viewModel);
        }
    }
}
