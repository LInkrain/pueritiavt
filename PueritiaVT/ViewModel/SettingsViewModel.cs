﻿using PueritiaVT.Interfaces;
using PueritiaVT.Models;
using PueritiaVT.Properties;
using System;
using System.IO;
using System.Threading.Tasks;

namespace PueritiaVT.ViewModel
{
    public class SettingsViewModel : ViewModelBase
    {
        private SettingsApp testSettings;
        private SettingsApp testSettingsOriginal;
        private readonly IWindowService windowService;
        private readonly IFileSettingsService fileService;
        private readonly IMsgBoxService msgBoxService;
        private RelayCommand openCommand;
        private RelayCommand saveCommand;
        private RelayCommand cancelCommand;
        private bool Saved = false;

        public SettingsApp TestSettings
        {
            get
            {
                return testSettings;
            }
            set
            {
                testSettings = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand OpenCommand
        {
            get
            {
                return openCommand ??
                  (openCommand = new RelayCommand(obj =>
                  {
                      if (Directory.Exists(SettingsApp.SettingsFolderPath))
                      {                        
                          try
                          {
                              string fPath = SettingsApp.SettingsFolderPath + SettingsApp.SettingsFolderPath + fileService.FileExtension;
                              TestSettings = fileService.Open(fPath);
                          }
                          catch
                          {
                              msgBoxService.ShowNotification(Resources.ErrorSettingsLoading);
                          }
                      }
                  }));
            }
        }

        public RelayCommand SaveCommand
        {
            get
            {
                return saveCommand ??
                  (saveCommand = new RelayCommand(obj =>
                  {
                      if (!Directory.Exists(SettingsApp.SettingsFolderPath))
                      {
                          Directory.CreateDirectory(SettingsApp.SettingsFolderPath);
                      }
                      try
                      {
                          string fPath = SettingsApp.SettingsFolderPath + SettingsApp.SettingsFileName + fileService.FileExtension;
                          SaveSettingsAsync(fPath, TestSettings);
                          this.Saved = true;
                          windowService.CurrentView?.Close();
                      }
                      catch
                      {
                          msgBoxService.ShowNotification(Resources.ErrorSettingsSaving);
                      }
                  }));
            }
        }

        private async void SaveSettingsAsync(string fPath, SettingsApp TestSettings)
        {
            await Task.Run(() => fileService.Save(fPath, TestSettings));
        }

        public RelayCommand CancelCommand
        {
            get
            {
                return cancelCommand ??
                  (cancelCommand = new RelayCommand(obj =>
                  {
                      if (!this.Saved)
                      {
                          testSettingsOriginal.Clone(TestSettings);
                      }
                      windowService.CurrentView?.Close();
                  }));
            }
        }        

        public SettingsViewModel(IWindowService windowService, IFileSettingsService fileService, IMsgBoxService msgBoxService, SettingsApp testSettings)
        {
            testSettingsOriginal = new SettingsApp();
            TestSettings =  testSettings;
            testSettings.Clone(testSettingsOriginal);
            this.windowService = windowService;
            this.fileService = fileService;
            this.msgBoxService = msgBoxService;            
        }
    }
}
