﻿using PueritiaVT.Interfaces;
using PueritiaVT.Models;
using PueritiaVT.Properties;
using PueritiaVT.Services;
using PueritiaVT.View;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace PueritiaVT.ViewModel
{
    public class ChooseQuestionsNumbersViewModel : ViewModelBase, IDataErrorInfo
    {
        private readonly IWindowService windowService;
        private QuestionsList testQuestionsList;
        private SettingsApp testSettings;
        private int fromNumber;
        private int toNumber;
        private RelayCommand startTestCommand;
        private RelayCommand exitCommand;

        //Реализация IDataErrorInfo
        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                switch (columnName)
                {
                    case "FromNumber":
                        if ((FromNumber <= 0) || (FromNumber > TestQuestionsList.Questions.Count))
                        {
                            error = Resources.ErrorValid1;
                        }
                        break;
                    case "ToNumber":
                        if ((ToNumber <= 0) || (ToNumber > TestQuestionsList.Questions.Count))
                        {
                            error = Resources.ErrorValid1;
                        }
                        break;                    
                }
                return error;
            }
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        //Общее количество вопросов в файле
        public int TotalQuestionsCount
        {
            get
            {
                return TestQuestionsList.Questions.Count;
            }
        }

        //Список вопросов
        public QuestionsList TestQuestionsList
        {
            get
            {
                return testQuestionsList;
            }
            set
            {
                testQuestionsList = value;
                OnPropertyChanged();
            }
        }

        //Номер с которого начинать
        public int FromNumber
        {
            get
            {
                return fromNumber;
            }
            set
            {
                fromNumber = value;
                OnPropertyChanged();
            }
        }

        //Номер по который юрать вопросы
        public int ToNumber
        {
            get
            {
                return toNumber;
            }
            set
            {
                toNumber = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand StartTestCommand
        {
            get
            {
                return startTestCommand ??
                  (startTestCommand = new RelayCommand(obj =>
                  {
                      TestQuestionsList.Questions = new ObservableCollection<Question>(TestQuestionsList.Questions.Skip(FromNumber - 1).Take(ToNumber - FromNumber+1));
                      TestQuestionsList.RandQuestions(testSettings.Randomize);
                      windowService.CreateChildWindow<TestingWindow>();
                      windowService.ShowDialogWindow(new TestingViewModel(new WindowService(windowService), new MsgBoxService(), TestQuestionsList, testSettings));
                  }));
            }
        }

        public RelayCommand ExitCommand
        {
            get
            {
                return exitCommand ??
                  (exitCommand = new RelayCommand(obj =>
                  {
                      windowService.CurrentView?.Close();
                  }));
            }
        }

        public ChooseQuestionsNumbersViewModel(IWindowService windowService,QuestionsList testQuestionsList, SettingsApp testSettings)
        {
            this.windowService = windowService;
            this.TestQuestionsList = testQuestionsList;
            this.testSettings = testSettings;
            this.FromNumber = 1;
            this.ToNumber = TotalQuestionsCount;
        }
    }
}
