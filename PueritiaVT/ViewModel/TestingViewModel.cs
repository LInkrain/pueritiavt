﻿using PueritiaVT.Interfaces;
using PueritiaVT.Models;
using PueritiaVT.Properties;
using PueritiaVT.Services;
using PueritiaVT.View;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Threading;

namespace PueritiaVT.ViewModel
{
    public class TestingViewModel : ViewModelBase
    {
        private readonly IWindowService windowService;
        private readonly IMsgBoxService msgBoxService;
        private QuestionsList testQuestionsList;
        public QuestionsList WrongQuestions { get; set; }
        private DispatcherTimer dispatcherTimer;
        private int seconds = 60;
        private int lives = 0;
        private int questionNumber;
        private SettingsApp testSettings;
        private Question currentQuestion;
        private RelayCommand nextQuestionCommand;
        private RelayCommand exitCommand;
        private Answer selectedAnswer;
        private RelayCommand selectAnswerCommand;

        public Answer SelectedAnswer
        {
            get
            {
                return selectedAnswer;                  
            }
            set
            {
                selectedAnswer = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand ExitCommand
        {
            get
            {
                return exitCommand ??
                  (exitCommand = new RelayCommand(obj =>
                  {
                      windowService.CurrentView.Close();
                  }));
            }
        }

        public RelayCommand SelectAnswerCommand
        {
            get
            {
                return selectAnswerCommand ??
                  (selectAnswerCommand = new RelayCommand(obj =>
                  {
                      SelectedAnswer.Choose = !SelectedAnswer.Choose;
                  }));
            }
        }

        public RelayCommand NextQuestionCommand
        {
            get
            {
                return nextQuestionCommand ??
                  (nextQuestionCommand = new RelayCommand(obj =>
                  {
                      GiveAnswer();
                  }));
            }
        }

        public Question CurrentQuestion
        {
            get { return currentQuestion; }
            set
            {
                currentQuestion = value;
                OnPropertyChanged();
            }
        }

        //Номер вопроса в массиве
        public int QuestionNumber
        {
            get { return questionNumber; }
            set
            {
                questionNumber = value;
                OnPropertyChanged();
                OnPropertyChanged("CurrentNumber");
            }
        }
        public int TotalQuestionsCount
        {
            get
            {
                return TestQuestionsList.Questions.Count;
            }
        }

        public int Lives
        {
            get { return lives; }
            set
            {
                lives = value;
                OnPropertyChanged();
            }
        }

        public int Seconds
        {
            get { return seconds; }
            set
            {
                seconds = value;
                OnPropertyChanged();
            }
        }

        public SettingsApp TestSettings
        {
            get { return testSettings; }
            set
            {
                testSettings = value;
                OnPropertyChanged();
            }
        }

        //Номер вопроса, который отображается пользователю
        public int CurrentNumber
        {
            get { return QuestionNumber + 1; }
        }

        public QuestionsList TestQuestionsList
        {
            get
            {
                return testQuestionsList;
            }
            set
            {
                testQuestionsList = value;
                OnPropertyChanged();
            }
        }

        public TestingViewModel(IWindowService windowService, IMsgBoxService msgBoxService, QuestionsList testQuestionsList, SettingsApp testSettings)
        {
            this.windowService = windowService;
            this.msgBoxService = msgBoxService;
            TestQuestionsList = testQuestionsList;
            TestSettings = testSettings;
            Lives = TestSettings.Lives;
            WrongQuestions = new QuestionsList();
            WrongQuestions.FileName = Resources.WrongQuestions;
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(DispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            NextQuestionShow();
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            Seconds--;
            if (Seconds <= 0)
            {
                dispatcherTimer.Stop();
                GiveAnswer();
            }
        }

        private void StartTimer()
        {
            seconds = 60;
            dispatcherTimer.Start();
        }

        private void NextQuestionShow()
        {
            RandAnswer();
            CurrentQuestion = TestQuestionsList.Questions[QuestionNumber];
            SelectedAnswer = CurrentQuestion.AnswerList.First();
            if (TestSettings.Timer)
            {
                dispatcherTimer.Stop();
                StartTimer();
            }
        }

        private void RandAnswer()
        {
            if (TestSettings.Randomize)
            {
                testQuestionsList.RandAnswers(QuestionNumber);
            }
        }

        private void GiveAnswer()
        {
            dispatcherTimer.Stop();
            if (CheckAnswer())
            {
                RightAnswerGiven();
            }
            else
            {
                WrongAnswerGiven();
            }
        }

        private bool CheckAnswer()
        {
            bool rightAnswer = true;
            foreach (var item in CurrentQuestion.AnswerList)
            {
                if (((!item.Choose) && (item.Sign == "+") || ((item.Choose) && (item.Sign == "-"))))
                {
                    rightAnswer = false;
                }
            }
            return rightAnswer;
        }

        private void RightAnswerGiven()
        {
            if (TestSettings.ShowRightAnswers)
            {
                windowService.CreateChildWindow<AnswerWindow>();
                AnswerViewModel viewModel = new AnswerViewModel(new WindowService(windowService), CurrentQuestion.AnswerList, Resources.AllAnswersCorrect);
                windowService.ShowDialogWindow(viewModel);
                if ((bool)viewModel.DialogResult)
                {
                    NextAnswer();
                }
            }
            else
            {
                NextAnswer();
            }
        }

        private void WrongAnswerGiven()
        {
            if (!WrongQuestions.Questions.Contains(CurrentQuestion))
            {
                WrongQuestions.Questions.Add(CurrentQuestion);
                if (TestSettings.Examin)
                {
                    Lives--;
                }
            }
            windowService.CreateChildWindow<AnswerWindow>();
            AnswerViewModel viewModel = new AnswerViewModel(new WindowService(windowService), CurrentQuestion.AnswerList, Resources.Errors);
            windowService.ShowDialogWindow(viewModel);
            if ((bool)viewModel.DialogResult)
            {
                NextAnswer();
            }
        }

        private void NextAnswer()
        {           
            if (TestSettings.Examin)
            {
                     if (Lives <= 0)
                     {
                        StatisticPrint();
                     }
            }
            if (QuestionNumber < testQuestionsList.Questions.Count-1)
            {
                QuestionNumber++;
                NextQuestionShow();
            }
            else
            {
                StatisticPrint();
            }
        }

        private void StatisticPrint()
        {
            Enums.ResultStatisticFeedBack dialogResult;
            if (WrongQuestions.Questions.Count > 0)
            {
                windowService.CreateChildWindow<StatisticWindow>();
                StatisticViewModel viewModel = new StatisticViewModel(new WindowService(windowService), WrongQuestions, TestQuestionsList);              
                windowService.ShowDialogWindow(viewModel);
                dialogResult = viewModel.DialogResult;
            }
            else
            {
                windowService.CreateChildWindow<CongratulationWindow>();
                CongratulationViewModel viewModel = new CongratulationViewModel(new WindowService(windowService));               
                windowService.ShowDialogWindow(viewModel);
                dialogResult = viewModel.DialogResult;
            }
            switch (dialogResult)
            {
                case Enums.ResultStatisticFeedBack.Again:
                    {
                        Restart(TestQuestionsList);
                        break;
                    }
                case Enums.ResultStatisticFeedBack.Wrong:
                    {
                        Restart(WrongQuestions);
                        break;
                    }
                case Enums.ResultStatisticFeedBack.Exit:
                    {
                        windowService.CurrentView.Close();
                        windowService.ParentWindowService.CurrentView.Close();
                        break;
                    }
            }
        }

        private void Restart(QuestionsList newQuestionsList = null)
        {
            TestQuestionsList = newQuestionsList;
            Lives = TestSettings.Lives;
            WrongQuestions = new QuestionsList
            {
                FileName = Resources.Errors
            };
            QuestionNumber = 0;
            NextQuestionShow();
        }
    }
}
