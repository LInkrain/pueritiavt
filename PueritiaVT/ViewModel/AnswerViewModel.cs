﻿using PueritiaVT.Interfaces;
using PueritiaVT.Models;
using System.Collections.ObjectModel;

namespace PueritiaVT.ViewModel
{
    public class AnswerViewModel : ViewModelBase
    {
        IWindowService windowService;
        public string Title {get;}
        public bool DialogResult { get; set; }
        public ObservableCollection<Answer> AnswerList { get; set; }
        private RelayCommand nextCommand;
        private RelayCommand returnCommand;

        public RelayCommand NextCommand
        {
            get
            {
                return nextCommand ??
                  (nextCommand = new RelayCommand(obj =>
                  {
                      this.DialogResult = true;
                      windowService.CurrentView?.Close();
                  }));
            }
        }

        public RelayCommand ReturnCommand
        {
            get
            {
                return returnCommand ??
                  (returnCommand = new RelayCommand(obj =>
                  {
                      this.DialogResult = false;
                      windowService.CurrentView?.Close();
                  }));
            }
        }

        public AnswerViewModel(IWindowService windowService, ObservableCollection<Answer> answerList, string title)
        {
            this.windowService = windowService;
            AnswerList = answerList;
            Title = title;
        }
    }
}
