﻿using PueritiaVT.Interfaces;
using PueritiaVT.Models;
using PueritiaVT.Properties;
using System;

namespace PueritiaVT.ViewModel
{    
    class StatisticViewModel:ViewModelBase
    {
        private readonly IWindowService windowService;
        private QuestionsList testQuestionsList;
        private QuestionsList wrongQuestionsList;
        private RelayCommand againCommand;
        private RelayCommand exitCommand;
        private RelayCommand wrongTestsCommand;

        public string PerCent { get; set; }
        public string FileName { get; set; }
        public Enums.ResultStatisticFeedBack DialogResult { get; set; }      

        public QuestionsList TestQuestionsList
        {
            get { return testQuestionsList; }
            set
            {
                testQuestionsList = value;
                OnPropertyChanged();
            }
        }

        public QuestionsList WrongQuestionsList
        {
            get { return wrongQuestionsList; }
            set
            {
                wrongQuestionsList = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand WrongTestsCommand
        {
            get
            {
                return wrongTestsCommand ??
                  (wrongTestsCommand = new RelayCommand(obj =>
                  {
                      DialogResult = Enums.ResultStatisticFeedBack.Wrong;
                      windowService.CurrentView?.Close();
                  }));
            }
        }

        public RelayCommand AgainCommand
        {
            get
            {
                return againCommand ??
                  (againCommand = new RelayCommand(obj =>
                  {
                      DialogResult = Enums.ResultStatisticFeedBack.Again;
                      windowService.CurrentView?.Close();
                  }));
            }
        }

        public RelayCommand ExitCommand
        {
            get
            {
                return exitCommand ??
                  (exitCommand = new RelayCommand(obj =>
                  {
                      DialogResult = Enums.ResultStatisticFeedBack.Exit;
                      windowService.CurrentView?.Close();
                  }));
            }
        }

        public StatisticViewModel(IWindowService windowService, QuestionsList wrongQuestionsList, QuestionsList testQuestionsList)
        {
            this.windowService = windowService;
            this.WrongQuestionsList = wrongQuestionsList;
            this.TestQuestionsList = testQuestionsList;
            this.FileName = WrongQuestionsList.FileName;
            PerCent = (Math.Round(((float)(TestQuestionsList.Questions.Count - WrongQuestionsList.Questions.Count) / TestQuestionsList.Questions.Count) * 100, 2)).ToString();
            PerCent = Resources.CorrectAnswers + " : " + PerCent + "%";
        }
    }
}
