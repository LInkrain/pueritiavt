﻿using PueritiaVT.Interfaces;
using PueritiaVT.Models;
using PueritiaVT.Properties;
using PueritiaVT.Services;
using PueritiaVT.View;
using System;
using System.IO;
using System.Windows;

namespace PueritiaVT.ViewModel
{
    public class MainMenuViewModel : ViewModelBase
    {
        private readonly IWindowService windowService;
        private readonly IDialogService dialogService;
        private readonly IFileSettingsService fileService;
        private readonly IMsgBoxService msgBoxService;
        private SettingsApp testSettings;
        private RelayCommand chooseTestCommand;
        private RelayCommand settingsCommand;
        private RelayCommand aboutCommand;
        private RelayCommand exitCommand;
        private RelayCommand openCommand;
        private QuestionsList testQuestionsList;
                
        public QuestionsList TestQuestionsList
        {
            get
            {
                return testQuestionsList;
            }
            set
            {
                testQuestionsList = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand ExitCommand
        {
            get
            {
                return exitCommand ??
                  (exitCommand = new RelayCommand(obj =>
                  {
                      Application.Current.MainWindow.Close();
                  }));
            }
        }

        public RelayCommand AboutCommand
        {
            get
            {
                return aboutCommand ??
                  (aboutCommand = new RelayCommand(obj =>
                  {
                      windowService.CreateChildWindow<AboutWindow>();
                      windowService.ShowDialogWindow(new AboutViewModel());
                  }));
            }
        }

        public RelayCommand SettingsCommand
        {
            get
            {
                return settingsCommand ??
                  (settingsCommand = new RelayCommand(obj =>
                  {
                      windowService.CreateChildWindow<SettingsWindow>();
                      windowService.ShowDialogWindow(new SettingsViewModel(new WindowService(windowService), new JsonFileSettingsService(), new MsgBoxService(), testSettings));
                  }));
            }
        }

        public RelayCommand ChooseTestCommand
        {
            get
            {
                return chooseTestCommand ??
                  (chooseTestCommand = new RelayCommand(obj =>
                  {
                      try
                      {
                          if (dialogService.OpenFileDialog() == true)
                          {
                              TestQuestionsList = new QuestionsList(dialogService.FilePath,dialogService.FileName);
                              windowService.CreateChildWindow<ChooseQuestionsNumbersWindow>();
                              ChooseQuestionsNumbersViewModel viewModel = new ChooseQuestionsNumbersViewModel(new WindowService(windowService), TestQuestionsList,testSettings);
                              windowService.ShowDialogWindow(viewModel);
                          }
                       }
                      catch (Exception ex)
                {
                    dialogService.ShowMessage(ex.Message);
                }
            }));
            }
        }

        public RelayCommand OpenCommand
        {
            get
            {
                return openCommand ??
                  (openCommand = new RelayCommand(obj =>
                  {
                      if (Directory.Exists(SettingsApp.SettingsFolderPath))
                      {
                          try
                          {
                              string fPath = SettingsApp.SettingsFolderPath + SettingsApp.SettingsFileName + fileService.FileExtension;
                              testSettings = fileService.Open(fPath);
                          }
                          catch
                          {
                              msgBoxService.ShowNotification(Resources.ErrorSettingsLoading);
                              testSettings = new SettingsApp();
                          }
                      }
                      else
                      {
                          testSettings = new SettingsApp();
                      }
                  }));
            }
        }
        
        public MainMenuViewModel(IWindowService windowService, IDialogService dialogService, IMsgBoxService msgBoxService, IFileSettingsService fileService)
        {
            this.windowService = windowService;
            this.dialogService = dialogService;
            this.msgBoxService = msgBoxService;
            this.fileService = fileService;
        }
    }    
}
