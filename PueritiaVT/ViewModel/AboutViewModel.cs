﻿using PueritiaVT.Models;
using System;
using System.Reflection;
using System.Configuration;

namespace PueritiaVT.ViewModel
{
    public class AboutViewModel: ViewModelBase
    {
        private RelayCommand linkAndroidCommand;
        private RelayCommand linkVKGroupCommand;
        private RelayCommand linkEmailCommand;

        public string AndroidLink
        {
            get
            {
                return ConfigurationManager.AppSettings["AndroidLink"];
            }
        }

        public string SupportEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["SupportEmail"];
            }
        }

        public string VKLink
        {
            get
            {
                return ConfigurationManager.AppSettings["VKLink"];
            }
        }

        public RelayCommand LinkAndroidCommand
        {
            get
            {
                return linkAndroidCommand ??
                  (linkAndroidCommand = new RelayCommand(obj =>
                  {
                      System.Diagnostics.Process.Start(AndroidLink);
                  }));
            }
        }
        public RelayCommand LinkVKGroupCommand
        {
            get
            {
                return linkVKGroupCommand ??
                  (linkVKGroupCommand = new RelayCommand(obj =>
                  {
                      System.Diagnostics.Process.Start(VKLink);
                  }));
            }
        }
        public RelayCommand LinkEmailCommand
        {
            get
            {
                return linkEmailCommand ??
                  (linkEmailCommand = new RelayCommand(obj =>
                  {
                    System.Diagnostics.Process.Start(String.Format("mailto:{0}?subject={1}&cc={2}&bcc={3}&body={4}",
                    SupportEmail, "", "", "", ""));
                  }));
            }
        }

        public string Version
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }
    }
}
