﻿using PueritiaVT.Interfaces;
using PueritiaVT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PueritiaVT.ViewModel
{
   public class CongratulationViewModel:ViewModelBase
    {
        private readonly IWindowService windowService;
        private RelayCommand againCommand;
        private RelayCommand exitCommand;
        public Enums.ResultStatisticFeedBack DialogResult;

        public RelayCommand AgainCommand
        {
            get
            {
                return againCommand ??
                  (againCommand = new RelayCommand(obj =>
                  {
                      DialogResult = Enums.ResultStatisticFeedBack.Again;
                      windowService.CurrentView?.Close();
                  }));
            }
        }

        public RelayCommand ExitCommand
        {
            get
            {
                return exitCommand ??
                  (exitCommand = new RelayCommand(obj =>
                  {
                      DialogResult = Enums.ResultStatisticFeedBack.Exit;
                      windowService.CurrentView?.Close();
                  }));
            }
        }

        public CongratulationViewModel(IWindowService windowService)
        {
            this.windowService = windowService;
        }
    }
}
