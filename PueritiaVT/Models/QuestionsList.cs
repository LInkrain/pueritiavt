﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PueritiaVT.Models
{
    public class QuestionsList
    {
        public ObservableCollection<Question> Questions{ get; set; }
        public string FileName { get; set; }
        public QuestionsList(string path, string fileName)
        {
            Questions = new ObservableCollection<Question>();
            GetQuestionsList(path,fileName);
        }
        public QuestionsList()
        {
            Questions = new ObservableCollection<Question>();            
        }
        private ObservableCollection<Question> GetQuestionsList(string path,string fileName)
        {
            FileName = fileName;
            Questions?.Clear();
            string text = "";
            text = File.ReadAllText(path, Encoding.Unicode);

            List<string> lines = text.Split('#').ToList();

            if (lines[0][0] != '#')
            {
                lines.RemoveAt(0);
            }

            foreach (var item in lines)
            {
                List<string> qstn = item.Split(new string[] { "\r\n" }, StringSplitOptions.None).ToList();
                Question question = new Question() { Number = qstn[0], Text = qstn[1] };
                for (int i = 2; i < qstn.Count; i++)
                {
                    if (qstn[i] != "")
                    {
                        if ((qstn[i][0] == '+') || (qstn[i][0] == '-'))
                        {
                            Answer answ = new Answer();
                            answ.Sign = qstn[i][0].ToString();                           
                            answ.Text = qstn[i].TrimStart('+', '-',' ').TrimEnd();
                            question.AnswerList.Add(answ);
                        }
                    }
                }
                Questions.Add(question);
            }
            return Questions;
        }

        public void RandQuestions(bool randomize)
        {
            if (randomize)
            {
                System.Random rand = new System.Random();
                Questions = new ObservableCollection<Question>(Questions.OrderBy(c => rand.Next()).ToList());
            }
        }

        public void RandAnswers(int QuestionNumber)
        {
                System.Random rand = new System.Random();
                Questions[QuestionNumber].AnswerList = new ObservableCollection<Answer>(Questions[QuestionNumber].AnswerList.OrderBy(c => rand.Next()).ToList());
        }
    }
}
