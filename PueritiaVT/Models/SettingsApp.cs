﻿using PueritiaVT.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace PueritiaVT.Models
{
    public class SettingsApp : ViewModelBase
    {
        public static string SettingsFolderPath = ConfigurationManager.AppSettings["SettingsFolderPath"];
        public static string SettingsFileName = ConfigurationManager.AppSettings["SettingsFileName"];
        bool randomize = false;
        bool showRightAnswers = false;
        bool timer = false;
        bool examin = false;
        int lives = 1;
        public bool Randomize
        {
            get { return randomize; }
            set
            {
                randomize = value;
                OnPropertyChanged("Randomize");
            }
        }
        public bool ShowRightAnswers
        {
            get { return showRightAnswers; }
            set
            {
                showRightAnswers = value;
                OnPropertyChanged("ShowRightAnswers");
            }
        }
        public bool Timer
        {
            get { return timer; }
            set
            {
                timer = value;
                OnPropertyChanged("Timer");
            }
        }
        public bool Examin
        {
            get { return examin; }
            set
            {
                examin = value;
                OnPropertyChanged("Examin");
            }
        }
        public int Lives
        {
            get { return lives; }
            set
            {
                lives = value;
                OnPropertyChanged("Lives");
            }
        }

        public SettingsApp Clone(SettingsApp origin, bool deep = false)
        {
            if (deep)
            {
                origin = new SettingsApp();
            }
            origin.Examin = this.Examin;
            origin.Lives = this.Lives;
            origin.Randomize = this.Randomize;
            origin.ShowRightAnswers = this.ShowRightAnswers;
            origin.Timer = this.Timer;
            return origin;
        }
    }
}
