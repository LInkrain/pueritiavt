﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PueritiaVT.Models
{
    public class Answer : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private string text;
        private string sign;
        private bool choose;

        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                OnPropertyChanged("Text");
            }
        }
        public string Sign
        {
            get { return sign; }
            set
            {
                sign = value;
                OnPropertyChanged("Sign");
            }
        }
        public bool Choose
        {
            get { return choose; }
            set
            {
                choose = value;
                OnPropertyChanged("Choose");
            }
        }

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
    public class Question : INotifyPropertyChanged
    {
        private ObservableCollection<Answer> answerList;
        private string text;
        private string number;
        public event PropertyChangedEventHandler PropertyChanged;

        public string Number
        {
            get { return number; }
            set
            {
                number = value;
                OnPropertyChanged("Number");
            }
        }
        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                OnPropertyChanged("Text");
            }
        }
        public ObservableCollection<Answer> AnswerList
        {
            get
            {
                return answerList;
            }
            set
            {
                answerList = value;
                OnPropertyChanged("AnswerList");
            }
        }
        
        public Question()
        {
            AnswerList = new ObservableCollection<Answer>();
        }

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
