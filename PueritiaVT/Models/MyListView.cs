﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PueritiaVT.Models
{
        public class ListViewItem : System.Windows.Controls.ListViewItem
        {
            protected override void OnSelected(RoutedEventArgs e)
            {
                base.OnSelected(e);

                if (IsSelected)
                {
                    Focus();
                }
            }
        }
    public class ListView : System.Windows.Controls.ListView
    {
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new ListViewItem();
        }
    }
}
